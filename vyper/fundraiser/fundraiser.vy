# @version ^0.2.7
owner:     public(address)
target:    public(uint256)
endTime:   public(uint256)
funds:     public(uint256)
Mk:        uint256


struct Contributor:
    userAddress: address
    contribution: uint256
    
contributors: HashMap[uint256, Contributor]


@external
@payable
def __init__(_target: uint256, duration: uint256):
    self.owner = msg.sender
    self.target = _target
    self.endTime = block.timestamp + duration
    self.Mk = 0


@internal
def contribute(Sender: address, Value: uint256):
    assert block.timestamp < self.endTime, "Funding period closed"

    self.contributors[self.Mk] = Contributor({
    userAddress: Sender,
    contribution: Value
    })
    self.Mk += 1


@external
def collect():
    assert self.balance >= self.target, "Funding target not met"
    assert msg.sender == self.owner, "Only owner can collect"
    selfdestruct(self.owner)


@external
def refund():
    assert block.timestamp > self.endTime, "Funding period not closed"
    assert self.balance < self.target, "Funding target met"
    for k in range(MAX_UINT256):
        if k == self.Mk:
           break
        send(self.contributors[k].userAddress, self.contributors[k].contribution)
    selfdestruct(self.owner)


@external
@payable
def __default__():
    self.contribute(msg.sender, msg.value)
    
